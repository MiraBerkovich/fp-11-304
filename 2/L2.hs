
-- Названия функций: буквы,
-- строчные и заглавные, цифры, подчеркивание
-- Должно начинаться со строчной буквы (или _)
-- Арифметические операции - как обычно (+,-,*,скобки)
fN4_me :: Integer -> Integer -> Integer
fN4_me arg1 arg2 = (arg1+arg2)*2

-- Вызовы: имя функции, через пробел аргументы
-- В REPL набрать :reload (:r) -- файл перезагрузится
f2 x = 1 + fN4_me x x

{- REPL, types:
*Main> :t fN4_me
fN4_me :: Num a => a -> a -> a
*Main> :t f2
f2 :: Num a => a -> a
*Main> f2 "a"

<interactive>:9:1:
    No instance for (Num [Char]) arising from a use of ‘f2’
    In the expression: f2 "a"
    In an equation for ‘it’: it = f2 "a"
*Main> f2 (+1)

<interactive>:10:1:
    Non type-variable argument in the constraint: Num (a -> a)
    (Use FlexibleContexts to permit this)
    When checking that ‘it’ has the inferred type
      it :: forall a. (Num a, Num (a -> a)) => a -> a
*Main>
-}

-- В функцию можно передавать функцию
g f x = f (f x)

g2 = fN4_me 1
-- на самом деле:
g2'' x = fN4_me 1 x
-- или даже так:
g2' = \x -> fN4_me 1 x

g3 y = fN4_me y 1

-- Лямбда-функция:
g3' = \y -> fN4_me y 1
-- \арг арг ... -> тело

-- Рекурсия!
fact 0 = 1
fact n = n * fact (n-1)
-- = 5 * fact 4
-- = 5 * 4 * fact 3
-- = 5 * 4 * 3 * fact 2
-- = 5 * 4 * 3 * 2 * fact 1
-- = 5 * 4 * 3 * 2 * 1 * fact 0
-- = 5 * 4 * 3 * 2 * 1 * 1
-- = 120

-- pattern matching
h 0 x = x
h 1 x = 2*x
h x 0 = x*3
h x 1 = x*4
h x y = y*h (x-1) 0
h x y = 5

-- pattern guards
h2 x y | x==y      = x
       | otherwise = 0

h3 x | x == 1 = 132123
     | True   = 42

-- otherwise = False

h4 x | x==4 = x +
              x
              + x*x / (x+1)
     | x==5 = 7
h4 x = x

h6 x = x*h5

h5 = 7


