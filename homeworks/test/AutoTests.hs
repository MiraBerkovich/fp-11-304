module AutoTests where

-- Do not change! It will be overwritten!
-- Use MyTests.hs to define your tests

import Test.Tasty (TestTree(..), testGroup)
import Test.Tasty.HUnit

import Lib

exampleTests :: [TestTree]
exampleTests =
  [ testGroup "HW 0"
    [ testGroup "HW 0.0"
      [ testCase "Works for Alice" $ hw0_0 "Alice" @?= "Hello, Alice"
      , testCase "Works for Bob"   $ hw0_0 "Bob"   @?= "Hello, Bob"
      ]
    ]
  , testGroup "HW 1"
    [ testGroup "HW 1.1"
      [ testCase "Works for 2 and 3"    $ hw1_1 2 3    @?= 5
      , testCase "Works for 3 and 1000" $ hw1_1 3 1000 @?= 1003
      ]
    ]
  ]

autoTests :: [TestTree]
autoTests = [ ]

